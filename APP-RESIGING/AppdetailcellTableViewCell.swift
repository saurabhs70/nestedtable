//
//  AppdetailcellTableViewCell.swift
//  APP-RESIGING
//
//  Created by saurabh-pc on 05/11/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class AppdetailcellTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var viewOuter: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func update(cell:AppdetailcellTableViewCell)
    {
        cell.viewOuter.layer.cornerRadius = 5
         cell.viewOuter.layer.borderColor = UIColor.black.cgColor

        // border
        cell.viewOuter.layer.borderWidth = 1.0
        // cell.viewOuther.layer.borderColor = UIColor.black.cgColor
        
        // shadow
        //cell.viewOuther.layer.shadowColor = UIColor.black.cgColor
        // cell.viewOuther.layer.shadowOffset = CGSize(width: 3, height: 3)
        //cell.viewOuther.layer.shadowOpacity = 0.7
        //cell.viewOuther.layer.shadowRadius = 4.0
        cell.viewOuter.clipsToBounds=true

    }

}
