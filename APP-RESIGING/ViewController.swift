//
//  ViewController.swift
//  APP-RESIGING
//
//  Created by saurabh-pc on 05/11/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var sections = sectionData


    @IBOutlet weak var listTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listTbl.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view, typically from a nib.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item: Section = sections[section]
        if item.items.count >= 1 {
            return 1
        }
        return 0//sections.count
    }
    
    // cell height
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let item: Section = sections[indexPath.section]
let size=120 * item.items.count+80
        if(item.collapsed==true){
            print(CGFloat(size))
            return CGFloat(size)
        }
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = listTbl.dequeueReusableCell(withIdentifier: "AppCellTableViewCellId", for: indexPath ) as! AppCellTableViewCell
        cell.update(cell: cell,arrlist:sections,indexpath:indexPath)
        
        // connect objects with our information from arrays
        //cell.usernameBtn.setTitle(usernameArray[indexPath.row], for: UIControlState.normal)
        //cell.usernameBtn.sizeToFit()
        //cell.commentLbl.text = commentArray[indexPath.row]
        return cell
    }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         var item: Section = sections[indexPath.section]
        if item.collapsed==true {
            item.collapsed=false

        }
        else
        {
        item.collapsed=true
        }
        //item.collapsed=true
        sections.remove(at: indexPath.section)
        sections.insert(item, at:  indexPath.section)
        let item2: Section = sections[indexPath.section]
        print(item2.collapsed)
        listTbl.reloadData()

        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

