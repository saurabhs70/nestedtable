//
//  Data.swift
//  APP-RESIGING
//
//  Created by saurabh-pc on 05/11/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import Foundation

public struct Item
{
    var name:String
    var detail:String
    
   public init(nameIs:String,detailIs:String) {
        name=nameIs
        detail=detailIs
    }
}

public struct Section
{
    var name:String
    var items:[Item]
    var collapsed: Bool
   public init(nameIs:String,itemsIs:[Item],collapsedIs:Bool=false) {
        name=nameIs
        items=itemsIs
        collapsed=collapsedIs
    }
}

public var sectionData:[Section]=[
  Section(nameIs: "first", itemsIs: [Item(nameIs: "MacBook1", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook2", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook3", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook4", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook5", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity.")]),
  Section(nameIs: "second", itemsIs: [Item(nameIs: "MacBook pro1", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook pro2", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook pro3", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook pro4", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook pro5", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook pro6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity.")]),
  Section(nameIs: "third", itemsIs: [Item(nameIs: "MacBook Air1", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Air2", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Air3", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Air4", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Air5", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Air6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity.")]),
  Section(nameIs: "forth", itemsIs: [Item(nameIs: "MacBook mini1", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Mini2", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Mini3", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Mini4", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Mini5", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity."),Item(nameIs: "MacBook Mini6", detailIs: "Apple's ultraportable laptop, trading portability for speed and connectivity.")])
    

]
