//
//  AppCellTableViewCell.swift
//  APP-RESIGING
//
//  Created by saurabh-pc on 05/11/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class AppCellTableViewCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var internals:[Item]?
    
    @IBOutlet weak var viewBorder: UIView!
    
    @IBOutlet weak var viewcolor: UIView!
    
    @IBOutlet weak var listTbl: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewOuther: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func update(cell:AppCellTableViewCell,arrlist:[Section],indexpath:IndexPath)  {
        listTbl.delegate=self
        listTbl.dataSource=self
        cell.viewOuther.layer.cornerRadius = 5
        
        // border
        //cell.viewOuther.layer.borderWidth = 1.0
       // cell.viewOuther.layer.borderColor = UIColor.black.cgColor
        
        // shadow
        //cell.viewOuther.layer.shadowColor = UIColor.black.cgColor
       // cell.viewOuther.layer.shadowOffset = CGSize(width: 3, height: 3)
        //cell.viewOuther.layer.shadowOpacity = 0.7
        //cell.viewOuther.layer.shadowRadius = 4.0
                cell.viewOuther.clipsToBounds=true
        let val=arrlist[indexpath.section]
        if val.collapsed==true {
            //viewBorder.isHidden=true
            //viewcolor.backgroundColor=UIColor.init(colorLiteralRed: 196/255, green: 199/255, blue: 206/255, alpha: 1)
        }
        else
        {
            //viewBorder.isHidden=false

            //viewcolor.backgroundColor=UIColor.white
        }
        //view color
        
        internals=arrlist[indexpath.section].items
        listTbl.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (internals?.count)! //sections.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = listTbl.dequeueReusableCell(withIdentifier: "AppdetailcellTableViewCellId", for: indexPath ) as! AppdetailcellTableViewCell
        cell.update(cell: cell)
        let value = internals?[indexPath.row]
        cell.lblTitle.text=value?.name
        // connect objects with our information from arrays
        //cell.usernameBtn.setTitle(usernameArray[indexPath.row], for: UIControlState.normal)
        //cell.usernameBtn.sizeToFit()
        //cell.commentLbl.text = commentArray[indexPath.row]
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        
    }

}
